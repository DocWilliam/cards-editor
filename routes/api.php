<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\CrudController;
use App\Http\Controllers\HydrateController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/tokens/create', function (Request $request) {
    $token = $request->user()->createToken($request->token_name);

    return ['token' => $token->plainTextToken];
});

// HYDRATE
Route::get('/restaurants/{userId}',[HydrateController::class, 'getRestaurantsByUser']);
Route::get('/cards/{restaurantId}',[HydrateController::class, 'getCardsByRestaurant']);
Route::get('/sections/{cardId}',[HydrateController::class, 'getSectionsByCard']);
Route::get('/menus/{cardId}',[HydrateController::class, 'getMenusByCard']);
Route::get('/dishessection/{sectionId}',[HydrateController::class, 'getDishesBySection']);
Route::get('/dishesmenu/{menuId}',[HydrateController::class, 'getDishesByMenu']);
Route::get('/allergens',[HydrateController::class, 'getAllergens']);

Route::get('/dishinfo/{dishId}',[HydrateController::class, 'getDishInfoById']);
Route::get('/menuinfo/{menuId}',[HydrateController::class, 'getMenuInfoById']);
Route::get('/sectioninfo/{sectionId}',[HydrateController::class, 'getSectionInfoById']);
Route::get('/cardinfo/{cardId}',[HydrateController::class, 'getCardInfoById']);
Route::get('/restaurantinfo/{restaurantId}',[HydrateController::class, 'getRestaurantInfoById']);
Route::get('/restaurant/{restaurantUrl}',[HydrateController::class, 'getRestaurantInfoByUrl']);
Route::get('/card/{cardId}',[HydrateController::class, 'getCardById']);

// CREATE
Route::post('/createrestaurant',[CrudController::class, 'createRestaurant']);
Route::post('/createcard',[CrudController::class, 'createCard']);
Route::post('/createsection',[CrudController::class, 'createSection']);
Route::post('/createmenu',[CrudController::class, 'createMenu']);
Route::post('/createdishsection',[CrudController::class, 'createDishOnSection']);
Route::post('/createdishmenu',[CrudController::class, 'createDishOnMenu']);


// UPDATE
Route::post('/updaterestaurant',[CrudController::class, 'updateRestaurant']);
Route::post('/updatecard',[CrudController::class, 'updateCard']);
Route::post('/updatesection',[CrudController::class, 'updateSection']);
Route::post('/updatemenu',[CrudController::class, 'updateMenu']);
Route::post('/updatedish',[CrudController::class, 'updateDish']);
Route::post('/insertdishmenu',[CrudController::class, 'insertDishMenu']);
Route::post('/insertdishsection',[CrudController::class, 'insertDishSection']);
Route::post('/movedish',[CrudController::class, 'moveDish']);


// DELETE
Route::post('/deleterestaurant',[CrudController::class, 'deleteRestaurant']);
Route::post('/deletecard',[CrudController::class, 'deleteCard']);
Route::post('/deletesection',[CrudController::class, 'deleteSection']);
Route::post('/deletemenu',[CrudController::class, 'deleteMenu']);
Route::post('/deletedish',[CrudController::class, 'deleteDish']);


// TOGGLE
Route::post('/togglerestaurant',[CrudController::class, 'toggleRestaurant']);
Route::post('/togglecard',[CrudController::class, 'toggleCard']);
Route::post('/togglesection',[CrudController::class, 'toggleSection']);
Route::post('/togglemenu',[CrudController::class, 'toggleMenu']);
Route::post('/toggledish',[CrudController::class, 'toggleDish']);