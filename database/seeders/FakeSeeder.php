<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class FakeSeeder extends Seeder
{
    public function run()
    {
        $this->call(UsersFakeSeeder::class);
        $this->call(RestaurantsFakeSeeder::class);
        $this->call(CardsFakeSeeder::class);
        $this->call(MenusFakeSeeder::class);
        $this->call(SectionsFakeSeeder::class);
        $this->call(DishesFakeSeeder::class);
        $this->call(AllergensFakeSeeder::class);
        $this->call(AllergenDishesFakeSeeder::class);
        $this->call(MenuDishesFakeSeeder::class);
        $this->call(SectionDishesFakeSeeder::class);
    }
}
