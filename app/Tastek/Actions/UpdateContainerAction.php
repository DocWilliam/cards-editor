<?php

namespace App\Tastek\Actions;

use App\Tastek\Interfaces\Container;
use App\Models\Dish;
use Illuminate\Http\Request;

class UpdateContainerAction{

    protected $container;

    public function __construct(Container $container){

        $this->container =  $container;

    }

    public function run(Request $request){

        return $this->container->updateData($request);

    }
}