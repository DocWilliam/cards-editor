<?php

namespace App\Tastek\Actions;

use App\Tastek\Interfaces\Container;
use App\Models\Dish;

class InsertDishAction{

    protected $container;

    public function __construct(Container $container){

        $this->container =  $container;

    }

    public function run(int $dishId){

        $dish = Dish::find($dishId)->first();

        return $this->container->saveDish($dish);
    }
}