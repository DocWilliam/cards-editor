<?php

namespace App\Tastek\Actions;

use App\Tastek\Interfaces\Container;
use App\Models\Dish;
use Illuminate\Http\Request;

class CreateDishAction{

    protected $container;

    public function __construct(Container $container){

        $this->container =  $container;

    }

    public function run(Request $request){

        $dish = new Dish;

        $dish->name = $request->name;
        $dish->description = $request->description;
        $dish->price = $request->price;
        $dish->vegetarian = $request->vegetarian;
        $dish->vegan = $request->vegan;
        $dish->gluten_free = $request->glutenFree;
        $dish->active = true;

        return $this->container->saveDish($dish);
    }

}