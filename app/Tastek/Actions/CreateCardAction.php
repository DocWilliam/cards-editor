<?php

namespace App\Tastek\Actions;

use App\Models\Card;
use Illuminate\Http\Request;

class CreateCardAction{

    public function run(Request $request){

        $card = new Card;
    
        $card->name = $request->name;
        $card->restaurant_id = $request->restaurantId;
        $card->time_start = $request->timeStart;
        $card->time_end = $request->timeEnd;
        $card->active = true;
    
        return array('error'=>!$card->save(),'data'=>$card);
    }


}