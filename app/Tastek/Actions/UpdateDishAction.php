<?php

namespace App\Tastek\Actions;

use App\Models\Dish;
use Illuminate\Http\Request;

class UpdateDishAction{

    public function run(Request $request){

        $dish = Dish::find($request->dishId);

        $dish->name = $request->name;
        $dish->description = $request->description;
        $dish->price = $request->price;
        $dish->vegetarian = $request->vegetarian;
        $dish->vegan = $request->vegan;
        $dish->gluten_free = $request->gluten_free;
        $dish->save();
        return $dish;
    }

}