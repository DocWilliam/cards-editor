<?php

namespace App\Tastek\Actions;

use Illuminate\Http\Request;
use App\Models\Restaurant;

class UpdateRestaurantAction{

    public function run(Request $request){

        $restaurant = Restaurant::find($request->restaurantId);       

        $restaurant->name = $request->name;
        $restaurant->user_id = $request->userId;
        $restaurant->location = $request->location;
        $restaurant->description = $request->description;
        $restaurant->opening_hours = $request->openingHours;
        $restaurant->url = urlize($request->name);
        $restaurant->save();
        return $restaurant;

    }
}