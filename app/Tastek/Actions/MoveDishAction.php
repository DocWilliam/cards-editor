<?php

namespace App\Tastek\Actions;

use App\Tastek\Interfaces\Container;

class MoveDishAction{

    protected $from;
    protected $to;

    public function __construct(Container $from, Container $to){

        $this->from = $from;
        $this->to = $to;

    }


    public function run($dishId){

        $this->from->release($dishId);
        return $this->to->link($dishId);
    }

}