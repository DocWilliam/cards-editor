<?php

namespace App\Tastek\Actions;

use App\Models\Restaurant;
use App\Models\User;
use Illuminate\Http\Request;


class CreateRestaurantAction{

    public function run(Request $request){

        try {
            $restaurant = new Restaurant;
            $user = User::find($request->userId);
    
            if($user){
                $restaurant->name = $request->name;
                $restaurant->user_id = $request->userId;
                $restaurant->location = $request->location;
                $restaurant->description = $request->description;
                $restaurant->opening_hours = $request->openingHours;
                $restaurant->url = urlize($request->name);
                $restaurant->active = true;
        
                return array('error'=>!$restaurant->save(),'data'=>$restaurant);        
            }
            else{
                return array('error'=>true,'data'=>'User does not exist');
            }
        } catch (\Throwable $th) {
            return array('error'=>true,'data'=>$th->errorInfo);
        }

    }

}