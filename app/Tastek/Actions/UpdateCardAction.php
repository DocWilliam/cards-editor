<?php

namespace App\Tastek\Actions;

use App\Models\Card;
use Illuminate\Http\Request;

class UpdateCardAction{

    public function run(Request $request){

        $card = Card::find($request->cardId);

        $card->name = $request->name;
        $card->time_start = $request->time_start;
        $card->time_end = $request->time_end;
        $card->save();
        return $card;
    }

}