<?php

namespace App\Tastek\Actions;

use App\Models\Section;
use Illuminate\Http\Request;

class CreateSectionAction{

    public function run(Request $request){

        $section = new Section;
    
        $section->name = $request->name;
        $section->description = $request->description;
        $section->card_id = $request->cardId;
        $section->active = true;
    
        return array('error'=>!$section->save(),'data'=>$section);

    }

}