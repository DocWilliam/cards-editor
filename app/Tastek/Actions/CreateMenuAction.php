<?php

namespace App\Tastek\Actions;

use App\Models\Menu;
use Illuminate\Http\Request;

class CreateMenuAction{

    public function run(Request $request){

        $menu = new Menu;

        $menu->name = $request->name;
        $menu->description = $request->description;
        $menu->card_id = $request->cardId;
        $menu->price = $request->price;
        $menu->active = true;

        return array('error'=>!$menu->save(),'data'=>$menu);

    }

}