<?php

namespace App\Tastek\Interfaces;

use App\Models\Dish;
use Illuminate\Http\Request;

interface Container{

    public function saveDish(Dish $dish);
    public function updateData(Request $request);
    public function release(int $dihsId);
    public function link(int $dihsId);

}