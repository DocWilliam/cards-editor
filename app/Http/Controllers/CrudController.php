<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Restaurant;
use App\Models\Card;
use App\Models\Section;
use App\Models\Dish;
use App\Models\Menu;
use App\Tastek\Actions\CreateCardAction;
use App\Tastek\Actions\CreateDishAction;
use App\Tastek\Actions\CreateMenuAction;
use App\Tastek\Actions\CreateRestaurantAction;
use App\Tastek\Actions\CreateSectionAction;
use App\Tastek\Actions\InsertDishAction;
use App\Tastek\Actions\MoveDishAction;
use App\Tastek\Actions\UpdateCardAction;
use App\Tastek\Actions\UpdateContainerAction;
use App\Tastek\Actions\UpdateDishAction;
use App\Tastek\Actions\UpdateRestaurantAction;

class CrudController extends Controller
{

    // CREATE

    public function createRestaurant(Request $request, CreateRestaurantAction $action){
        
        return $action->run($request);
    }

    public function createCard(Request $request, CreateCardAction $action){

        return $action->run($request);
    }

    public function createSection(Request $request, CreateSectionAction $action){

        return $action->run($request);
    }

    public function createMenu(Request $request, CreateMenuAction $action){

        return $action->run($request);
    }

    public function createDishOnSection(Request $request, Dish $dish){

        $section = Section::find($request->menuId)->first();

        $actionCreate = new CreateDishAction($section);

        $connection = $actionCreate->run($request);

        return array('error'=>!$connection,'data'=>$dish);
    }

    public function createDishOnMenu(Request $request, Dish $dish){

        $menu = Menu::find($request->menuId)->first();

        $actionCreate = new CreateDishAction($menu);

        $connection = $actionCreate->run($request);

        return array('error'=>!$connection,'data'=>$dish);
    }

    // UPDATE

    public function updateRestaurant(Request $request, UpdateRestaurantAction $action){  
        
        $restaurant = $action->run($request);

        return array('error'=>!$restaurant,'data'=>$restaurant);        
    }

    public function updateCard(Request $request, UpdateCardAction $action){

        $card = $action->run($request);

        return array('error'=>!$card,'data'=>$card);
    }

    public function updateSection(Request $request){

        $section = Section::find($request->sectionId);

        $action = new UpdateContainerAction($section);

        $response = $action->run($request);

        return array('error'=>!$response,'data'=>$response);
    }

    public function updateMenu(Request $request){

        $menu = Section::find($request->menuId);

        $action = new UpdateContainerAction($menu);

        $response = $action->run($request);

        return array('error'=>!$response,'data'=>$response);
    }

    public function updateDish(Request $request, UpdateDishAction $action){

        $dish = $action->run($request);

        return array('error'=>!$dish,'data'=>$dish);
    }

    public function moveDish(Request $request){

        $from = Menu::find($request->fromId) ?? Section::find($request->fromId);
        $to = Menu::find($request->toId) ?? Section::find($request->toId);

        $action = new MoveDishAction($from,$to);

        $response = $action->run($request->dishId);
        
        return array('error'=>!$response,'data'=>$response);
    }

    public function insertDishSection(Request $request){

        $section = Section::find($request->sectionId)->first();
        $action = new InsertDishAction($section);

        $response = $action->run($request->dishId);

        return array('error'=>!$response->save(),'data'=>$response);
    }

    public function insertDishMenu(Request $request){

        $menu = Section::find($request->menuId)->first();
        $action = new InsertDishAction($menu);

        $response = $action->run($request->dishId);

        return array('error'=>!$response->save(),'data'=>$response);
    }

    // TOGGLE

    public function toggleRestaurant(Request $request){
        return array('error'=>false,'data'=>Restaurant::find($request->dishId)->toggle());
    }

    public function toggleCard(Request $request){
        return array('error'=>false,'data'=>Card::find($request->cardId)->toggle());
    }

    public function toggleSection(Request $request){
        return array('error'=>false,'data'=>Section::find($request->sectionId)->toggle());
    }

    public function toggleMenu(Request $request){
        return array('error'=>false,'data'=>Menu::find($request->menuId)->toggle());
    }

    public function toggleDish(Request $request){
        return array('error'=>false,'data'=>Dish::find($request->dishId)->toggle());
    }
}