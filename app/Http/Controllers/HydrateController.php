<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Restaurant;
use App\Models\Card;
use App\Models\Section;
use App\Models\Allergen;
use App\Models\Dish;
use App\Models\Menu;


class HydrateController extends Controller
{

    // MULTIPLE

    public function getRestaurantsByUser(Request $request,$userId){

        return array('error'=>false,'data'=>User::find($userId)->restaurants);

    }

    public function getCardsByRestaurant(Request $request,$restaurantId){

        return array('error'=>false,'data'=>Restaurant::find($restaurantId)->cards);

    }

    public function getSectionsByCard(Request $request,$cardId){

        return array('error'=>false,'data'=>Card::find($cardId)->sections);

    }

    public function getMenusByCard(Request $request,$cardId){

        return array('error'=>false,'data'=>Card::find($cardId)->menus);

    }

    public function getDishesBySection(Request $request,$sectionId){

        return array('error'=>false,'data'=>Section::find($sectionId)->dishes());

    }

    public function getDishesByMenu(Request $request,$menuId){

        return array('error'=>false,'data'=>Menu::find($menuId)->dishes());

    }

    public function getAllergens(Request $request){

        return array('error'=>false,'data'=>Allergen::get());

    }

    // SINGLE


    public function getDishInfoById(Request $request,$dishId){

        return array('error'=>false,'data'=>Dish::find($dishId)->dishInfo());

    }

    public function getMenuInfoById(Request $request,$menuId){

        $menu = Menu::find($menuId);

        return array('error'=>false,'data'=>array('menu'=>$menu,'dishes'=>$menu->dishes()));

    }

    public function getSectionInfoById(Request $request,$sectionId){

        $section = Section::find($sectionId);

        return array('error'=>false,'data'=>array('section'=>$section,'dishes'=>$section->dishes()));

    }

    public function getCardInfoById(Request $request,$cardId){

        return array('error'=>false,'data'=>Card::find($cardId)->get());

    }

    public function getRestaurantInfoById(Request $request,$restaurantId){

        return array('error'=>false,'data'=>Restaurant::find($restaurantId));

    }

    public function getRestaurantInfoByUrl(Request $request,$restaurantUrl){

        return array('error'=>false,'data'=>Restaurant::where('url',$restaurantUrl)->first());

    }

    public function getCardById(Request $request,$cardId){

        return array('error'=>false,'data'=>Card::find($cardId)->fullCard());

    }
}
