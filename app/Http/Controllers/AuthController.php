<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Hash;

class AuthController extends Controller
{
    public function login(Request $request){
    
        $user = User::where('email', $request->email)->first();
    
            if ($user &&
                Hash::check($request->password, $user->password)) {
        
                    $token = $user->createToken('access');
                    $user->auth_token = $token->plainTextToken;
                    $user->save();
                    $user->fresh();
        
                    $user->token = $token->plainTextToken;
                    $user->error =  false;
        
                    return $user;                       
            }
            else
                return array('error'=>true,'code'=>401);    
    }
}


