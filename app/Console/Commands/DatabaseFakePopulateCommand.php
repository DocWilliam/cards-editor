<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DatabaseFakePopulateCommand extends Command
{
        protected $signature = 'db:fake';
    
        public function handle()
        {
            $this->callSilently('migrate:fresh');
            $this->callSilently('db:seed',['--class' => 'FakeSeeder']);
        }
    
}