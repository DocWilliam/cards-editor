<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\SectionDish;
use App\Tastek\Interfaces\Container;
use Illuminate\Http\Request;

class Section extends Model implements Container
{
    use HasFactory;

    public function dishes(){
        return $this->hasMany(SectionDish::class)->get()->map(function($relation){
            return Dish::find($relation->dish_id);
        });
    }

    public function toggle(){
        $this->active = !$this->active;
        $this->save();
    }

    public function saveDish(Dish $dish){

        $connection = new SectionDish();
        
        $connection->menu_id = $this->id;
        $connection->dish_id = $dish->id;
        $connection->save();

        return $connection;

    }

    public function updateData(Request $request){
        $this->name = $request->name;
        $this->description = $request->description;
        $this->card_id = $request->cardId;
        $this->save();
        return $this;
    }

    public function release(int $dishId){

        $connection = SectionDish::where(['dish_id'=>$dishId,'section_id'=>$this->id]);

        $connection->delete();

    }

    public function link(int $dishId){

        $connection = new SectionDish();

        $connection->section_id = $this->id;
        $connection->dish_id = $dishId;
        $connection->save();

        return $connection;

    }
}
