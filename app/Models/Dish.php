<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Allergen;
use App\Models\AllergenDish;

class Dish extends Model
{
    use HasFactory;

    public function allergens(){

        return $this->hasMany(AllergenDish::class)->get()->map(function($relation){
            return Allergen::find($relation->allergen_id);
        });

    }

    public function dishInfo(){
        return array('dish'=>$this,'allergens'=>$this->allergens());
    }

    public function toggle(){
        $this->active = !$this->active;
        $this->save();
    }
}
