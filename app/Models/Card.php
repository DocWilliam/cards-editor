<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Section;
use App\Models\Dish;
use App\Models\Menu;

class Card extends Model
{
    use HasFactory;

    public function sections(){
        return $this->hasMany(Section::class);
    }

    public function menus(){
        return $this->hasMany(Menu::class);
    }

    public function dishes(){

        $menus = $this->menus();
        $sections = $this->menus();
        $response = array();

        foreach ($menus as $menu) {
            $response[] = $menu->dishes();
        }

        foreach ($sections as $section) {
            $response[] = $section->dishes();
        }

        return $response;
    }

    public function children(){
        return array('menus'=>$this->hasMany(Dish::class)->get(),'sections'=>$this->hasMany(Section::class)->get());
    }

    public function fullCard(){
        $response = array();

        $response['card'] = $this;
        $response['menus'] = $this->menus();
        $response['sections'] = $this->sections();
        $response['dishes'] = $this->menus();

        return $response;
    }

    public function toggle(){
        $this->active = !$this->active;
        $this->save();
    }
}
